let pokemonTypes = {};
let tableParentId = 'table';
let detailsParentId = 'details';
let sorting = 'id';

let page = 1;
const pageSize = 15;
let pages = 1;

function setPage(newPage) {
    if (newPage <= 0) {
        page = 1;
    } else if (newPage > pages) {
        page = pages;
    } else {
        page = newPage;
    }

    const previousButton = document.getElementById('previous');
    if (page === 1) {
        previousButton.disabled = true;
    } else {
        previousButton.disabled = false;
    }

    const nextButton = document.getElementById('next');
    if (page >= pages) {
        nextButton.disabled = true;
    } else {
        newPage.disabled = false;
    }
    console.debug(`page set to ${page}`)
    updatePokemonTypesTable();
}

function getRowId(resource) {
    return `${resource.id}_row`
}

function updateDetails(pokemonTypeId) {
    const parent = document.getElementById(detailsParentId);

    if (pokemonTypeId < 0) {
        parent.innerHTML = ``;
        parent.innerText = 'no data';
        return;
    }

    const pt = pokemonTypes?.data?.find(item => item.id === pokemonTypeId);

    parent.innerHTML = `
    <div class="card" id="${pt.id}_card">
      <img src="${pt.imgUrl}" class="card-img-top" alt="${pt.classification}">
      <div class="card-body">
        <h5 class="card-title">${pt.name} #${pt.pokedexNumber}</h5>
        <p class="card-text">
            Japanese name: ${pt.japaneseName} </br>
            Classification: ${pt.classification} </br>
            Abilities: ${pt.abilities?.join(", ") || "none"} </br>
            Type: ${pt.primaryType}${pt.secondaryType ? " , " + pt.secondaryType : ""}
        </p>
      </div>
      <button type="button" class="btn btn-danger" onclick="removePokemon('${pt.id}')">Delete</button>
    </div>
    `
}

function removePokemon(id) {
    fetch(`/pokemon/api/pokemonTypes/${id}`, {
        method: 'DELETE'
    }).then( () => {
            updateDetails(-1);
            updatePokemonTypesTable();
        }
    ).catch(err => {
            console.error(`Unable to fetch Pokemon Types: ${err.status}`);
            console.error(err);
    });

}

function pokemonTypeToRow(pokemonType) {
    return `
    <tr id="${getRowId(pokemonType)}" onclick="updateDetails('${pokemonType?.id?.trim()}')">
        <th scope="row">${pokemonType.id}</th>
        <td>${pokemonType.name}</td>
        <td>#${pokemonType.pokedexNumber}</td>
        <td>${pokemonType.primaryType}</td>
        <td>${pokemonType.secondaryType || '-'}</td>
    </tr>
    `
}

function changeSorting(sort) {
    console.debug("Changed sorting to: " + sort)
    sorting = sort;
    updatePokemonTypesTable()
}

async function updatePokemonTypesTable() {
    try {
        let res = await fetch(`/pokemon/api/pokemonTypes?pageNumber=${page}&pageSize=${pageSize}&sortBy=${sorting}`)
        pokemonTypes = await res.json();
        createPokemonTypesTable();
    } catch (err) {
        console.error(`Unable to fetch Pokemon Types: ${err.status}`);
        console.error(err);
    }

    const items = pokemonTypes.meta.total;
    console.debug(`number of items: ${items}`);

    console.debug(pokemonTypes);

    pages = Math.floor(items / pageSize) + 1;
    console.debug(`pages: ${pages}`)
    document.getElementById('pages').innerText = `${page}/${pages}`;
}

function createPokemonTypesTable() {
    const tableParent = document.getElementById(tableParentId);
    tableParent.innerHTML = `
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Name</th>
                    <th scope="col">Pokedex Number</th>
                    <th scope="col">Primary Type</th>
                    <th scope="col">Secondary Type</th>
                </tr>
            </thead>
            <tbody>
                ${
        pokemonTypes.data.map(resource => `${pokemonTypeToRow(resource)}`).join("\n")
        || "no data"
    }
            </tbody>
        </table>
    `
}

